package com.example.dmitriy.project;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class Coordinates {
    float lon;
    float lat;

    public float getLon() {
        return lon;
    }
    public float getLat() {
        return  lat;
    }
    public void setLon(float lon) {
        this.lon = lon;
    }
    public void setLat(float lat) {
        this.lat = lat;
    }
}
