package com.example.dmitriy.project;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    TextView temp;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new ParseTask().execute();
        temp=(TextView)findViewById(R.id.textView);
        img=(ImageView)findViewById(R.id.img);
    }
    private class ParseTask extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        Bitmap resultImg;
        MyWeather myWeather=null;

        @Override
        protected String doInBackground(Void... params) {
            // получаем данные с внешнего ресурса
            try {
                URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=Volgograd,RU&units=metric&APPID=56fced4f5553e84900959e27007f6f9b");

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson = buffer.toString();
                //Переводим в классовую структуру
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                myWeather = gson.fromJson(resultJson, MyWeather.class);
                //Получаем картинку
                resultImg=getImage(myWeather.weather.get(0).icon+".png");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        protected Bitmap getImage(String code){
            byte[] imageBuf=null;
            try {
                URL url = new URL("http://openweathermap.org/img/w/" + code);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream is = urlConnection.getInputStream();
                return BitmapFactory.decodeStream(is);
            }
            catch (Throwable t) {
                t.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String jsonText) {
            super.onPostExecute(jsonText);
            try {
                temp.setText("Volgograd\n\n"+myWeather.weather.get(0).main+
                        "\nTemperature "+Float.toString(myWeather.main.temp)+
                        "°\nWind speed "+Float.toString(myWeather.wind.speed)+
                        " m/s\nClouds "+Float.toString(myWeather.clouds.all)+
                        "%\nPressure "+Float.toString(myWeather.main.pressure)+" hpa"
                        );
                img.setImageBitmap(resultImg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
