package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Weather;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class WeatherDeserializer implements JsonDeserializer<ArrayList<Weather>> {
    @Override
    public ArrayList<Weather> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<Weather> weathers = new ArrayList<Weather>();
        JsonArray jsonArray=json.getAsJsonArray();
        for(JsonElement weather : jsonArray){
            Weather w=new Weather();
            JsonObject jsonObject=(JsonObject)weather;
            w.setId((int)jsonObject.get("id").getAsInt());
            w.setMain((String)jsonObject.get("main").getAsString());
            w.setDescription((String) jsonObject.get("description").getAsString());
            w.setIcon((String)jsonObject.get("icon").getAsString());
            weathers.add(w);
        }
        return weathers;
    }
}
