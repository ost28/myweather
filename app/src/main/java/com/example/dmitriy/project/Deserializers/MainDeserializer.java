package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Coordinates;
import com.example.dmitriy.project.Main;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Dmitriy on 06.07.2017.
 */

public class MainDeserializer implements JsonDeserializer<Main> {
    @Override
    public Main deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Main main = new Main();
        JsonObject jsonObject = json.getAsJsonObject();
        main.setTemp(jsonObject.get("temp").getAsFloat());
        main.setHumidity(jsonObject.get("humidity").getAsInt());
        main.setPressure(jsonObject.get("pressure").getAsFloat());
        main.setTemp_min(jsonObject.get("temp_min").getAsFloat());
        main.setTemp_max(jsonObject.get("temp_max").getAsFloat());
        return main;
    }
}
