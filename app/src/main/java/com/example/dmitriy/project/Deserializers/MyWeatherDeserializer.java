package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Clouds;
import com.example.dmitriy.project.Coordinates;
import com.example.dmitriy.project.Main;
import com.example.dmitriy.project.MyWeather;
import com.example.dmitriy.project.Weather;
import com.example.dmitriy.project.Wind;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.internal.Streams;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Dmitriy on 06.07.2017.
 */

public class MyWeatherDeserializer implements JsonDeserializer<MyWeather> {
    @Override
    public MyWeather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MyWeather myWeather = new MyWeather();
        JsonObject jsonObject = json.getAsJsonObject();
        //Заполнение всех полей
        myWeather.setCoord((Coordinates) context.deserialize(jsonObject.get("coord"), Coordinates.class));
        myWeather.setWeather((ArrayList<Weather>) context.deserialize(jsonObject.get("weather"), Weather.class));
        myWeather.setBase((String)jsonObject.get("base").getAsString());
        myWeather.setMain((Main)context.deserialize(jsonObject.get("main"), Main.class));
        myWeather.setWind((Wind)context.deserialize(jsonObject.get("wind"), Wind.class));
        myWeather.setClouds((Clouds)context.deserialize(jsonObject.get("clouds"), Clouds.class));
        myWeather.setDt((int)jsonObject.get("dt").getAsInt());
        myWeather.setId((int)jsonObject.get("id").getAsInt());
        myWeather.setName((String)jsonObject.get("name").getAsString());
        myWeather.setCod((int)jsonObject.get("cod").getAsInt());
        return myWeather;
    }
}
