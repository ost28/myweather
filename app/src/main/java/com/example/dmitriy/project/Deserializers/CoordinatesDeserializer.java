package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Coordinates;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class CoordinatesDeserializer implements JsonDeserializer<Coordinates> {
    @Override
    public Coordinates deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Coordinates coordinates = new Coordinates();
        JsonObject jsonObject = json.getAsJsonObject();
        coordinates.setLon(jsonObject.get("lon").getAsFloat());
        coordinates.setLat(jsonObject.get("lat").getAsFloat());
        return coordinates;
    }
}
