package com.example.dmitriy.project;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class Wind {
    float speed;
    float deg;

    public void setSpeed(float speed) {
        this.speed = speed;
    }
    public void setDeg(float deg) {
        this.deg = deg;
    }
}
