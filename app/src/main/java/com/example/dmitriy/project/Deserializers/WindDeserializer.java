package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Wind;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Dmitriy on 06.07.2017.
 */

public class WindDeserializer implements JsonDeserializer<Wind> {
    @Override
    public Wind deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Wind wind = new Wind();
        JsonObject jsonObject = json.getAsJsonObject();
        wind.setSpeed(jsonObject.get("speed").getAsFloat());
        wind.setDeg(jsonObject.get("deg").getAsInt());
        return wind;
    }
}
