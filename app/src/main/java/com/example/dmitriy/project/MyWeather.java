package com.example.dmitriy.project;

import java.util.ArrayList;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class MyWeather {
    Coordinates coord;
    ArrayList<Weather> weather;
    String base;
    Main main;
    Wind wind;
    Clouds clouds;
    int dt;
    int id;
    String name;
    int cod;

    public  MyWeather(){
        this.coord=new Coordinates();
        this.weather=new ArrayList<Weather>();
        this.main=new Main();
        this.wind=new Wind();
        this.clouds=new Clouds();
    }

    public void setCoord(Coordinates coord) {
        this.coord = coord;
    }
    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }
    public void setBase(String base) {
        this.base = base;
    }
    public void setMain(Main main) {
        this.main = main;
    }
    public void setWind(Wind wind) {
        this.wind = wind;
    }
    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }
    public void setDt(int dt) {
        this.dt = dt;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setCod(int cod) {
        this.cod = cod;
    }
}