package com.example.dmitriy.project.Deserializers;

import com.example.dmitriy.project.Clouds;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Dmitriy on 06.07.2017.
 */

public class CloudsDeserializer implements JsonDeserializer<Clouds> {
    @Override
    public Clouds deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Clouds clouds = new Clouds();
        JsonObject jsonObject = json.getAsJsonObject();
        clouds.setAll(jsonObject.get("all").getAsInt());
        return clouds;
    }
}
