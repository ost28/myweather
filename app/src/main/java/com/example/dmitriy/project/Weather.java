package com.example.dmitriy.project;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class Weather {
    int id;
    String main;
    String description;
    String icon;

    public void setId(int id) {
        this.id = id;
    }
    public void setMain(String main) {
        this.main = main;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
}
