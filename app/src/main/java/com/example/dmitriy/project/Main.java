package com.example.dmitriy.project;

/**
 * Created by Dmitriy on 05.07.2017.
 */

public class Main {
    float temp;
    int humidity;
    float pressure;
    float temp_min;
    float temp_max;

    public void setTemp(float temp) {
        this.temp = temp;
    }
    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
    public void setPressure(float pressure) {
        this.pressure = pressure;
    }
    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }
    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }
}
